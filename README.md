# Emilie

`pip install emilie & emilie`

## How to Bump Version
Update version in `setup.py` file.
```
python3 -m pip install --user --upgrade setuptools wheel
python3 setup.py sdist bdist_wheel
ls dist/
```
This creates the two distributions needed.

## How to Release
Practice first:
```
python3 -m pip install --user --upgrade twine
python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*
```
Once you see it on test pypi, you can do it on the real one with
```
python3 -m twine upload dist/*
```
